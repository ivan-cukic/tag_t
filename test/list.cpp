/*
 *   Copyright (C) 2021-2022 Ivan Cukic <ivan AT cukic.co>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU  Lesser General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <vector>

#include "tag_t/list.h"

template<typename T>
using is_integral_t = typename std::is_integral<T>::type;

void test_list_size()
{
    using tag_t::list;
    using tag_t::error::bad_index;

    static_assert(list<>::size == 0);
    static_assert(list<int>::size == 1);
    static_assert(list<int, char>::size == 2);
}

void test_list_bad_index()
{
    using tag_t::list;
    using tag_t::error::bad_index;

    // Empty
    static_assert(std::is_same_v<list<>::at<0>, bad_index>);
    static_assert(std::is_same_v<list<>::at<1>, bad_index>);
    static_assert(std::is_same_v<list<>::at<42>, bad_index>);

    // Single item
    static_assert(!std::is_same_v<list<int>::at<0>, bad_index>);
    static_assert(std::is_same_v<list<int>::at<1>, bad_index>);
    static_assert(std::is_same_v<list<int>::at<42>, bad_index>);

    // Several items
    static_assert(!std::is_same_v<list<int, char, bool>::at<0>, bad_index>);
    static_assert(!std::is_same_v<list<int, char, bool>::at<1>, bad_index>);
    static_assert(std::is_same_v<list<int, char, bool>::at<42>, bad_index>);
}

void test_list_at()
{
    using tag_t::list;
    using tag_t::error::bad_index;

    // Single item
    static_assert(std::is_same_v<list<int>::at<0>, int>);
    static_assert(std::is_same_v<list<int>::at<1>, bad_index>);

    // Several items
    static_assert(std::is_same_v<list<int, char, bool>::at<0>, int>);
    static_assert(std::is_same_v<list<int, char, bool>::at<1>, char>);
    static_assert(std::is_same_v<list<int, char, bool>::at<2>, bool>);
    static_assert(std::is_same_v<list<int, char, bool>::at<3>, bad_index>);
}

void test_list_first()
{
    using tag_t::list;
    using tag_t::error::bad_index;

    // Empty
    static_assert(std::is_same_v<list<>::first, bad_index>);

    // Single item
    static_assert(std::is_same_v<list<int>::first, int>);

    // Several items
    static_assert(std::is_same_v<list<int, char, bool>::first, int>);
}

void test_list_contains()
{
    using tag_t::list;

    // Empty
    static_assert(!list<>::contains<int>);
    static_assert(!list<>::contains<double>);

    // Single item
    static_assert(list<int>::contains<int>);
    static_assert(!list<int>::contains<double>);

    // Several items
    static_assert(list<int, char, bool>::contains<int>);
    static_assert(list<int, char, bool>::contains<char>);
    static_assert(list<int, char, bool>::contains<bool>);
    static_assert(!list<int, char, bool>::contains<float>);
}

void test_list_contains_instantiation_of()
{
    using tag_t::list;

    // Empty
    static_assert(!list<>::contains_instantiation_of<std::vector>);

    // Single item
    static_assert(list<std::vector<int>>::contains_instantiation_of<std::vector>);
    static_assert(!list<int>::contains_instantiation_of<std::vector>);

    // Several items
    static_assert(list<std::vector<int>, char, bool>::contains_instantiation_of<std::vector>);
    static_assert(list<int, std::vector<char>, bool>::contains_instantiation_of<std::vector>);
    static_assert(list<int, char, std::vector<bool>>::contains_instantiation_of<std::vector>);
    static_assert(!list<int, char, bool>::contains_instantiation_of<std::vector>);
}

void test_list_any_of()
{
    using tag_t::list;

    // With std::is_integral (result is in ::type)
    // Empty
    static_assert(!list<>::any_of<std::is_integral>);

    // Single item
    static_assert(list<int>::any_of<std::is_integral>);
    static_assert(!list<std::vector<int>>::any_of<std::is_integral>);

    // Several items
    static_assert(list<std::vector<int>, char, bool>::any_of<std::is_integral>);
    static_assert(list<int, std::vector<char>, bool>::any_of<std::is_integral>);
    static_assert(list<int, char, std::vector<bool>>::any_of<std::is_integral>);
    static_assert(!list<float, double, std::vector<int>>::any_of<std::is_integral>);

    // With is_integral_t (result is the result, not inside ::type)
    // Empty
    static_assert(!list<>::any_of<is_integral_t>);

    // Single item
    static_assert(list<int>::any_of<is_integral_t>);
    static_assert(!list<std::vector<int>>::any_of<is_integral_t>);

    // Several items
    static_assert(list<std::vector<int>, char, bool>::any_of<is_integral_t>);
    static_assert(list<int, std::vector<char>, bool>::any_of<is_integral_t>);
    static_assert(list<int, char, std::vector<bool>>::any_of<is_integral_t>);
    static_assert(!list<float, double, std::vector<int>>::any_of<is_integral_t>);
}

void test_list_all_of()
{
    using tag_t::list;

    // With std::is_integral (result is in ::type)
    // Empty
    static_assert(list<>::all_of<std::is_integral>);

    // Single item
    static_assert(list<int>::all_of<std::is_integral>);
    static_assert(!list<std::vector<int>>::all_of<std::is_integral>);

    // Several items
    static_assert(list<int, char, bool>::all_of<std::is_integral>);
    static_assert(!list<int, float, bool>::all_of<std::is_integral>);
    static_assert(!list<int, char, std::vector<bool>>::all_of<std::is_integral>);
    static_assert(!list<float, double, std::vector<int>>::all_of<std::is_integral>);

    // With is_integral_t (result is the result, not inside ::type)
    // Empty
    static_assert(list<>::all_of<is_integral_t>);

    // Single item
    static_assert(list<int>::all_of<is_integral_t>);
    static_assert(!list<std::vector<int>>::all_of<is_integral_t>);

    // Several items
    static_assert(list<int, char, bool>::all_of<is_integral_t>);
    static_assert(!list<int, float, bool>::all_of<is_integral_t>);
    static_assert(!list<int, char, std::vector<bool>>::all_of<is_integral_t>);
    static_assert(!list<float, double, std::vector<int>>::all_of<is_integral_t>);
}

void test_list_prepend()
{
    using tag_t::list;

    // Empty
    static_assert(list<>::prepend<>{} == list<>{});
    static_assert(list<int>::prepend<>{} == list<int>{});
    static_assert(list<>::prepend<int>{} == list<int>{});
    static_assert(list<>::prepend<>::prepend<>{} == list<>{});

    // Single item
    static_assert(list<int>::prepend<char>{} == list<char, int>{});
    static_assert(list<int>::prepend<char, bool>{} == list<char, bool, int>{});
    static_assert(list<int, char>::prepend<bool>{} == list<bool, int, char>{});
    static_assert(list<int>::prepend<char>::prepend<bool>{} == list<bool, char, int>{});

    // Several items
    static_assert(list<int, char>::prepend<bool, float>{} == list<bool, float, int, char>{});
    static_assert(list<int, char, int>::prepend<bool, float, int>{} == list<bool, float, int, int, char, int>{});
    static_assert(list<int, char>::prepend<int, char>::prepend<int, bool>{} == list<int, bool, int, char, int, char>{});
}

void test_list_append()
{
    using tag_t::list;

    // Empty
    static_assert(list<>::append<>{} == list<>{});
    static_assert(list<int>::append<>{} == list<int>{});
    static_assert(list<>::append<int>{} == list<int>{});
    static_assert(list<>::append<>::append<>{} == list<>{});

    // Single item
    static_assert(list<int>::append<char>{} == list<int, char>{});
    static_assert(list<int>::append<char, bool>{} == list<int, char, bool>{});
    static_assert(list<int, char>::append<bool>{} == list<int, char, bool>{});
    static_assert(list<int>::append<char>::append<bool>{} == list<int, char, bool>{});

    // Several items
    static_assert(list<int, char>::append<bool, float>{} == list<int, char, bool, float>{});
    static_assert(list<int, char, int>::append<bool, float, int>{} == list<int, char, int, bool, float, int>{});
    static_assert(list<int, char>::append<int, char>::append<int, bool>{} == list<int, char, int, char, int, bool>{});
}

void test_list_concatenate()
{
    using tag_t::list;

    // Empty
    static_assert(list<>::concatenate<list<>>{} == list<>{});
    static_assert(list<int>::concatenate<list<>>{} == list<int>{});
    static_assert(list<>::concatenate<list<int>>{} == list<int>{});
    static_assert(list<>::concatenate<list<>>::concatenate<list<>>{} == list<>{});

    // Single item
    static_assert(list<int>::concatenate<list<char>>{} == list<int, char>{});
    static_assert(list<int>::concatenate<list<char, bool>>{} == list<int, char, bool>{});
    static_assert(list<int, char>::concatenate<list<bool>>{} == list<int, char, bool>{});
    static_assert(list<int>::concatenate<list<char>>::concatenate<list<bool>>{} == list<int, char, bool>{});

    // Several items
    static_assert(list<int, char>::concatenate<list<bool, float>>{} == list<int, char, bool, float>{});
    static_assert(list<int, char, int>::concatenate<list<bool, float, int>>{} == list<int, char, int, bool, float, int>{});
    static_assert(list<int, char>::concatenate<list<int, char>>::concatenate<list<int, bool>>{} == list<int, char, int, char, int, bool>{});
}

void test_list_filter()
{
    using tag_t::list;

    // With std::is_integral (result is in ::type)
    // Empty
    static_assert(list<>::filter<std::is_integral>{} == list<>{});

    // Single item
    static_assert(list<int>::filter<std::is_integral>{} == list<int>{});
    static_assert(list<std::vector<int>>::filter<std::is_integral>{} == list<>{});

    // Several items
    static_assert(list<std::vector<int>, char, bool>::filter<std::is_integral>{} == list<char, bool>{});
    static_assert(list<int, std::vector<char>, bool>::filter<std::is_integral>{} == list<int, bool>{});
    static_assert(list<int, char, std::vector<bool>>::filter<std::is_integral>{} == list<int, char>{});

    // With is_integral_t (result is the result, not inside ::type)
    // Empty
    static_assert(list<>::filter<is_integral_t>{} == list<>{});

    // Single item
    static_assert(list<int>::filter<is_integral_t>{} == list<int>{});
    static_assert(list<std::vector<int>>::filter<is_integral_t>{} == list<>{});

    // Several items
    static_assert(list<std::vector<int>, char, bool>::filter<is_integral_t>{} == list<char, bool>{});
    static_assert(list<int, std::vector<char>, bool>::filter<is_integral_t>{} == list<int, bool>{});
    static_assert(list<int, char, std::vector<bool>>::filter<is_integral_t>{} == list<int, char>{});
}

template<typename T>
struct cref {
    using type = const T &;
};

template<typename T>
using cref_t = const T &;

void test_list_transform_plain()
{
    using tag_t::list;

    // Empty
    static_assert(list<>::transform_plain<cref>{} == list<>{});

    // Single item
    static_assert(list<int>::transform_plain<cref>{} == list<cref<int>>{});
    static_assert(list<std::vector<int>>::transform_plain<cref>{} == list<cref<std::vector<int>>>{});

    // Several items
    static_assert(list<int, char, bool>::transform_plain<cref>{} == list<cref<int>, cref<char>, cref<bool>>{});
}

void test_list_transform_unwrap()
{
    using tag_t::list;

    // Empty
    static_assert(list<>::transform_unwrap<cref>{} == list<>{});

    // Single item
    static_assert(list<int>::transform_unwrap<cref>{} == list<cref_t<int>>{});
    static_assert(list<std::vector<int>>::transform_unwrap<cref>{} == list<cref_t<std::vector<int>>>{});

    // Several items
    static_assert(list<int, char, bool>::transform_unwrap<cref>{} == list<cref_t<int>, cref_t<char>, cref_t<bool>>{});
}

void test_list_transform()
{
    using tag_t::list;

    // Empty
    static_assert(list<>::transform<cref>{} == list<>{});

    // Single item
    static_assert(list<int>::transform<cref>{} == list<const int &>{});
    static_assert(list<std::vector<int>>::transform<cref>{} == list<const std::vector<int> &>{});

    // Several items
    static_assert(list<int, char, bool>::transform<cref>{} == list<const int &, const char &, const bool &>{});

    // Empty
    static_assert(list<>::transform<cref_t>{} == list<>{});

    // Single item
    static_assert(list<int>::transform<cref_t>{} == list<const int &>{});
    static_assert(list<std::vector<int>>::transform<cref_t>{} == list<const std::vector<int> &>{});

    // Several items
    static_assert(list<int, char, bool>::transform<cref_t>{} == list<const int &, const char &, const bool &>{});
}

void test_list_join()
{
    using tag_t::join;
    using tag_t::list;

    // Empty
    static_assert(join<list<>>{} == list<>{});
    static_assert(join<list<list<>>>{} == list<>{});
    static_assert(join<list<list<>, list<>>>{} == list<>{});

    // Single item
    static_assert(join<list<list<int>>>{} == list<int>{});
    static_assert(join<list<list<int>, list<>>>{} == list<int>{});
    static_assert(join<list<list<int>, list<char>>>{} == list<int, char>{});
    static_assert(join<list<list<int, char>, list<double>>>{} == list<int, char, double>{});

    // Several items
    static_assert(join<list<list<int>, list<char, double>, list<int, char, double>>>{} == list<int, char, double, int, char, double>{});
}
