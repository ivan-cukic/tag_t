/*
 *   Copyright (C) 2021-2022 Ivan Cukic <ivan AT cukic.co>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU  Lesser General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string>
#include <vector>

#include "tag_t/algorithm.h"
#include "tag_t/list.h"

void test_algorithm_contains()
{
    using tag_t::algorithm::contains_v;

    // Empty
    static_assert(!contains_v<std::string>);

    // One item
    static_assert(contains_v<std::string, std::string>);
    static_assert(!contains_v<std::string, int>);

    // Several items
    static_assert(contains_v<std::string, std::string, int, float>);
    static_assert(contains_v<std::string, int, std::string, float>);
    static_assert(contains_v<std::string, int, float, std::string>);
    static_assert(!contains_v<std::string, int, float>);
}

void test_algorithm_contains_instantiation()
{
    using tag_t::algorithm::contains_instantiation_of_v;

    // Empty
    static_assert(!contains_instantiation_of_v<std::basic_string>);

    // One item
    static_assert(contains_instantiation_of_v<std::basic_string, std::string>);
    static_assert(!contains_instantiation_of_v<std::basic_string, int>);
    static_assert(contains_instantiation_of_v<std::vector, std::vector<int>>);
    static_assert(!contains_instantiation_of_v<std::vector, int>);

    // Several items
    static_assert(contains_instantiation_of_v<std::basic_string, std::string, int, float>);
    static_assert(contains_instantiation_of_v<std::basic_string, int, std::string, float>);
    static_assert(contains_instantiation_of_v<std::basic_string, int, float, std::string>);
    static_assert(!contains_instantiation_of_v<std::basic_string, int, float>);
    static_assert(contains_instantiation_of_v<std::vector, std::vector<int>, int, float>);
    static_assert(contains_instantiation_of_v<std::vector, int, std::vector<int>, float>);
    static_assert(contains_instantiation_of_v<std::vector, int, float, std::vector<int>>);
    static_assert(!contains_instantiation_of_v<std::vector, int, float>);
}

void test_algorithm_any_of()
{
    using tag_t::algorithm::any_of_v;

    // Empty
    static_assert(!any_of_v<std::is_integral>);

    // One item
    static_assert(any_of_v<std::is_integral, int>);
    static_assert(!any_of_v<std::is_integral, std::string>);

    // Several items
    static_assert(any_of_v<std::is_integral, int, std::string, float>);
    static_assert(any_of_v<std::is_integral, std::string, int, float>);
    static_assert(any_of_v<std::is_integral, std::string, float, int>);
    static_assert(!any_of_v<std::is_integral, std::string, float>);
}

void test_algorithm_all_of()
{
    using tag_t::algorithm::all_of_v;

    // Empty
    static_assert(all_of_v<std::is_integral>);

    // One item
    static_assert(all_of_v<std::is_integral, int>);
    static_assert(!all_of_v<std::is_integral, std::string>);

    // Several items
    static_assert(!all_of_v<std::is_integral, int, std::string, float>);
    static_assert(!all_of_v<std::is_integral, std::string, int, float>);
    static_assert(!all_of_v<std::is_integral, std::string, float, int>);
    static_assert(all_of_v<std::is_integral, int, long>);
}

void test_algorithm_filter()
{
    using tag_t::list;
    using tag_t::algorithm::filter_t;

    // Empty
    static_assert(list<>{} == filter_t<std::is_integral, list>{});

    // One item
    static_assert(list<int>{} == filter_t<std::is_integral, list, int>{});
    static_assert(list<>{} == filter_t<std::is_integral, list, std::string>{});

    // Several items
    static_assert(list<>{} == filter_t<std::is_integral, list, double, std::string, float>{});
    static_assert(list<int>{} == filter_t<std::is_integral, list, int, std::string, float>{});
    static_assert(list<int>{} == filter_t<std::is_integral, list, std::string, int, float>{});
    static_assert(list<int>{} == filter_t<std::is_integral, list, std::string, float, int>{});
    static_assert(list<int, long>{} == filter_t<std::is_integral, list, int, std::string, long, float>{});
    static_assert(list<int, long>{} == filter_t<std::is_integral, list, std::string, int, float, long>{});
    static_assert(list<int, long>{} == filter_t<std::is_integral, list, std::string, float, int, long>{});
}

void test_algorithm_transform_plain_vector()
{
    using tag_t::list;
    using tag_t::algorithm::transform_plain_t;

    // Empty
    static_assert(list<>{} == transform_plain_t<std::vector, list>{});

    // One item
    static_assert(list<std::vector<int>>{} == transform_plain_t<std::vector, list, int>{});
    static_assert(list<std::vector<std::string>>{} == transform_plain_t<std::vector, list, std::string>{});

    // Several items
    static_assert(list<std::vector<double>, std::vector<std::string>, std::vector<float>>{}
                  == transform_plain_t<std::vector, list, double, std::string, float>{});
}

template<typename T>
using is_integral_t = typename std::is_integral<T>::type;

void test_algorithm_transform_plain_is_integral_t()
{
    using std::false_type;
    using std::true_type;
    using tag_t::list;
    using tag_t::algorithm::transform_plain_t;

    // Empty
    static_assert(list<>{} == transform_plain_t<is_integral_t, list>{});

    // One item
    static_assert(list<true_type>{} == transform_plain_t<is_integral_t, list, int>{});
    static_assert(list<false_type>{} == transform_plain_t<is_integral_t, list, std::string>{});

    // Several items
    static_assert(list<false_type, false_type, false_type>{} == transform_plain_t<is_integral_t, list, double, std::string, float>{});
    static_assert(list<true_type, false_type, false_type>{} == transform_plain_t<is_integral_t, list, int, std::string, float>{});
    static_assert(list<false_type, true_type, false_type>{} == transform_plain_t<is_integral_t, list, std::string, int, float>{});
    static_assert(list<false_type, false_type, true_type>{} == transform_plain_t<is_integral_t, list, std::string, float, int>{});
    static_assert(list<true_type, false_type, true_type, false_type>{} == transform_plain_t<is_integral_t, list, int, std::string, long, float>{});
    static_assert(list<false_type, true_type, false_type, true_type>{} == transform_plain_t<is_integral_t, list, std::string, int, float, long>{});
    static_assert(list<false_type, false_type, true_type, true_type>{} == transform_plain_t<is_integral_t, list, std::string, float, int, long>{});
}

void test_algorithm_transform_unwrap()
{
    using std::false_type;
    using std::true_type;
    using tag_t::list;
    using tag_t::algorithm::transform_unwrap_t;

    // Empty
    static_assert(list<>{} == transform_unwrap_t<std::is_integral, list>{});

    // One item
    static_assert(list<true_type>{} == transform_unwrap_t<std::is_integral, list, int>{});
    static_assert(list<false_type>{} == transform_unwrap_t<std::is_integral, list, std::string>{});

    // Several items
    static_assert(list<false_type, false_type, false_type>{} == transform_unwrap_t<std::is_integral, list, double, std::string, float>{});
    static_assert(list<true_type, false_type, false_type>{} == transform_unwrap_t<std::is_integral, list, int, std::string, float>{});
    static_assert(list<false_type, true_type, false_type>{} == transform_unwrap_t<std::is_integral, list, std::string, int, float>{});
    static_assert(list<false_type, false_type, true_type>{} == transform_unwrap_t<std::is_integral, list, std::string, float, int>{});
    static_assert(list<true_type, false_type, true_type, false_type>{} == transform_unwrap_t<std::is_integral, list, int, std::string, long, float>{});
    static_assert(list<false_type, true_type, false_type, true_type>{} == transform_unwrap_t<std::is_integral, list, std::string, int, float, long>{});
    static_assert(list<false_type, false_type, true_type, true_type>{} == transform_unwrap_t<std::is_integral, list, std::string, float, int, long>{});
}

void test_algorithm_transform_vector()
{
    using tag_t::list;
    using tag_t::algorithm::transform_t;

    // Empty
    static_assert(list<>{} == transform_t<std::vector, list>{});

    // One item
    static_assert(list<std::vector<int>>{} == transform_t<std::vector, list, int>{});
    static_assert(list<std::vector<std::string>>{} == transform_t<std::vector, list, std::string>{});

    // Several items
    static_assert(list<std::vector<double>, std::vector<std::string>, std::vector<float>>{} == transform_t<std::vector, list, double, std::string, float>{});
}

template<typename T>
using is_integral_t = typename std::is_integral<T>::type;

void test_algorithm_transform_is_integral_t()
{
    using std::false_type;
    using std::true_type;
    using tag_t::list;
    using tag_t::algorithm::transform_t;

    // Empty
    static_assert(list<>{} == transform_t<is_integral_t, list>{});

    // One item
    static_assert(list<true_type>{} == transform_t<is_integral_t, list, int>{});
    static_assert(list<false_type>{} == transform_t<is_integral_t, list, std::string>{});

    // Several items
    static_assert(list<false_type, false_type, false_type>{} == transform_t<is_integral_t, list, double, std::string, float>{});
    static_assert(list<true_type, false_type, false_type>{} == transform_t<is_integral_t, list, int, std::string, float>{});
    static_assert(list<false_type, true_type, false_type>{} == transform_t<is_integral_t, list, std::string, int, float>{});
    static_assert(list<false_type, false_type, true_type>{} == transform_t<is_integral_t, list, std::string, float, int>{});
    static_assert(list<true_type, false_type, true_type, false_type>{} == transform_t<is_integral_t, list, int, std::string, long, float>{});
    static_assert(list<false_type, true_type, false_type, true_type>{} == transform_t<is_integral_t, list, std::string, int, float, long>{});
    static_assert(list<false_type, false_type, true_type, true_type>{} == transform_t<is_integral_t, list, std::string, float, int, long>{});
}

void test_algorithm_transform()
{
    using std::false_type;
    using std::true_type;
    using tag_t::list;
    using tag_t::algorithm::transform_t;

    // Empty
    static_assert(list<>{} == transform_t<std::is_integral, list>{});

    // One item
    static_assert(list<true_type>{} == transform_t<std::is_integral, list, int>{});
    static_assert(list<false_type>{} == transform_t<std::is_integral, list, std::string>{});

    // Several items
    static_assert(list<false_type, false_type, false_type>{} == transform_t<std::is_integral, list, double, std::string, float>{});
    static_assert(list<true_type, false_type, false_type>{} == transform_t<std::is_integral, list, int, std::string, float>{});
    static_assert(list<false_type, true_type, false_type>{} == transform_t<std::is_integral, list, std::string, int, float>{});
    static_assert(list<false_type, false_type, true_type>{} == transform_t<std::is_integral, list, std::string, float, int>{});
    static_assert(list<true_type, false_type, true_type, false_type>{} == transform_t<std::is_integral, list, int, std::string, long, float>{});
    static_assert(list<false_type, true_type, false_type, true_type>{} == transform_t<std::is_integral, list, std::string, int, float, long>{});
    static_assert(list<false_type, false_type, true_type, true_type>{} == transform_t<std::is_integral, list, std::string, float, int, long>{});
}

template<typename C>
using value_type_t = typename C::value_type;

void test_algorithm_transform_value_type()
{
    using tag_t::list;
    using tag_t::algorithm::transform_t;

    static_assert(list<>{} == transform_t<std::vector, list>{});
    static_assert(list<int>{} == transform_t<value_type_t, list, std::vector<int>>{});
    static_assert(list<int, std::string>{} == transform_t<value_type_t, list, std::vector<int>, std::vector<std::string>>{});
    static_assert(list<int, std::string, char>{} == transform_t<value_type_t, list, std::vector<int>, std::vector<std::string>, std::vector<char>>{});
}

template<typename Key, typename Value>
struct map {
};

void test_algorithm_fold_left()
{
    using tag_t::list;
    using tag_t::algorithm::fold_left_t;

    static_assert(std::is_same_v<map<map<map<int, char>, double>, long>, fold_left_t<map, int, char, double, long>>);
}

void test_algorithm_fold_right()
{
    using tag_t::list;
    using tag_t::algorithm::fold_right_t;

    static_assert(std::is_same_v<map<map<map<long, int>, char>, double>, fold_right_t<map, long, double, char, int>>);
}

void test_algorithm_concat()
{
    using tag_t::list;
    using tag_t::algorithm::concat_t;

    // Empty
    static_assert(concat_t<list>{} == list<>{});
    static_assert(concat_t<list, list<>>{} == list<>{});
    static_assert(concat_t<list, list<>, list<>>{} == list<>{});
    static_assert(concat_t<list, list<>, list<>, list<>>{} == list<>{});

    // One
    static_assert(concat_t<list, list<int>>{} == list<int>{});
    static_assert(concat_t<list, list<>, list<int>>{} == list<int>{});
    static_assert(concat_t<list, list<>, list<int>, list<>>{} == list<int>{});

    // Several
    static_assert(concat_t<list, list<int>, list<char>>{} == list<int, char>{});
    static_assert(concat_t<list, list<int>, list<char, char>>{} == list<int, char, char>{});
    static_assert(concat_t<list, list<int>, list<char, char>, list<bool, char, int>>{} == list<int, char, char, bool, char, int>{});
}
