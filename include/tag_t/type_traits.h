/*
 *   Copyright (C) 2021-2022 Ivan Cukic <ivan AT cukic.co>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU  Lesser General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef IC_TAG_T_TYPE_TRAITS_H
#define IC_TAG_T_TYPE_TRAITS_H

#include <type_traits>
#include <utility>

#include "defines.h"

namespace tag_t::type_traits
{

// instantiation_of -- testing if a specified type is an instantiation
// of the specified template with some specific type parameters
// Note: will not match against templates with non-type parameters

template<typename Type, tagt_templatename Template>
struct is_instantiation_of : std::false_type {
};

template<typename... Args, tagt_templatename Template>
struct is_instantiation_of<Template<Args...>, Template> : std::true_type {
};

template<typename Type, tagt_templatename Template>
constexpr bool is_instantiation_of_v = is_instantiation_of<Type, Template>::value;

template<typename T, tagt_templatename Template>
concept instantiation_of = is_instantiation_of_v<T, Template>;

// Replacing the instantiation template

template<tagt_templatename To, template<typename...> typename From, typename... Values>
auto to_instantiation_impl(From<Values...> from) -> To<Values...>;

template<tagt_templatename To, typename From>
using to_instantiation_t = decltype(to_instantiation_impl(std::declval<From>()));

// Checks if a type is a value type (no const, no references)

template<typename T>
constexpr bool is_value_type_v = std::is_same_v<std::remove_cvref_t<T>, T>;

// Some useful meta-functions

template<typename T>
constexpr bool always_fail = false;

template<typename T>
using unwrap_t = typename T::type;

template<typename T>
struct wrap_t {
    using type = T;
};

template<tagt_templatename F, tagt_templatename G>
struct compose_t {
    template<typename T>
    using call = F<typename G<T>::type>;
};

} // namespace tag_t::type_traits

#endif // include guard
