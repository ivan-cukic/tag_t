/*
 *   Copyright (C) 2021-2022 Ivan Cukic <ivan AT cukic.co>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU  Lesser General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef IC_TAG_T_ALGORITHM_H
#define IC_TAG_T_ALGORITHM_H

#include <type_traits>
#include <utility>

#include "defines.h"
#include "type_traits.h"

namespace tag_t::algorithm
{

namespace details
{
template<typename T>
concept wrapped_type = requires { typename T::type; };

/*
 -- GCC doesn't like this
 -- Keeping this for the record - TODO: submit a bug report
template<typename T>
using auto_unwrap_impl = std::remove_pointer_t<decltype([] {
    if constexpr (wrapped_type<T>) {
        return (typename T::type *)nullptr;
    } else {
        return (T *)nullptr;
    }
}())>;
*/

template<typename T>
    requires wrapped_type<T>
auto auto_unwrap() -> typename T::type;

template<typename T>
    requires(!wrapped_type<T>)
auto auto_unwrap() -> T;

template<tagt_templatename Operation, typename Accumulator>
struct fold_wrapper_t {
    template<typename Value>
    auto operator%(Value) -> fold_wrapper_t<Operation, Operation<Accumulator, Value>>;

    using type = Accumulator;
};

template<tagt_templatename Operation, typename Accumulator, typename Value>
auto operator%(Value, fold_wrapper_t<Operation, Accumulator>) -> fold_wrapper_t<Operation, Operation<Accumulator, Value>>;

} // namespace details

template<tagt_templatename Operation, typename Init, typename... Values>
using fold_left_t = typename decltype((details::fold_wrapper_t<Operation, Init>{} % ... % std::declval<Values>()))::type;

template<tagt_templatename Operation, typename Init, typename... Values>
using fold_right_t = typename decltype((std::declval<Values>() % ... % details::fold_wrapper_t<Operation, Init>{}))::type;

template<typename Value, typename... Values>
constexpr bool contains_v = (std::is_same_v<Value, Values> || ... || false);

template<tagt_templatename Template, typename... Values>
constexpr bool contains_instantiation_of_v = (tag_t::type_traits::is_instantiation_of_v<Values, Template> || ... || false);

template<tagt_templatename Predicate, typename... Values>
constexpr bool any_of_v = (Predicate<Values>::value || ... || false);

template<tagt_templatename Predicate, typename... Values>
constexpr bool all_of_v = (Predicate<Values>::value && ... && true);

template<tagt_templatename Transformation, tagt_templatename TupleTemplate, typename... TupleItems>
using transform_plain_t = TupleTemplate<Transformation<TupleItems>...>;

template<tagt_templatename Transformation, tagt_templatename TupleTemplate, typename... TupleItems>
using transform_unwrap_t = TupleTemplate<typename Transformation<TupleItems>::type...>;

template<tagt_templatename Transformation, tagt_templatename TupleTemplate, typename... TupleItems>
using transform_t = TupleTemplate<decltype(details::auto_unwrap<Transformation<TupleItems>>())...>;

template<tagt_templatename TupleTemplate, typename... ValuesLeft, typename... ValuesRight>
auto concat_t_function_impl(TupleTemplate<ValuesLeft...>, TupleTemplate<ValuesRight...>) -> TupleTemplate<ValuesLeft..., ValuesRight...>;

template<typename Left, typename Right>
using concat_t_impl = decltype(concat_t_function_impl(std::declval<Left>(), std::declval<Right>()));

template<tagt_templatename TupleTemplate, typename... Tuples>
using concat_t = fold_left_t<concat_t_impl, TupleTemplate<>, Tuples...>;

template<tagt_templatename Predicate, tagt_templatename TupleTemplate, typename... TupleItems>
using filter_t = concat_t<TupleTemplate, std::conditional_t<Predicate<TupleItems>::value, TupleTemplate<TupleItems>, TupleTemplate<>>...>;

} // namespace tag_t

#endif // include guard
