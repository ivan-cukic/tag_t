/*
 *   Copyright (C) 2021-2022 Ivan Cukic <ivan AT cukic.co>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU  Lesser General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef IC_TAG_T_LIST_H
#define IC_TAG_T_LIST_H

#include <cstddef>
#include <tuple>
#include <type_traits>

#include "algorithm.h"
#include "debug.h"
#include "type_traits.h"

namespace tag_t
{

namespace error
{
struct bad_index {
};
} // namespace error

template<typename T>
concept list_like = requires { typename T::tag_t_list_instance; };

template<typename T>
using is_list = std::bool_constant<list_like<T>>;

template<typename... Values>
class list
{
private:
    // A null-terminated list of items
    using NullTerminatedValuesTuple = std::tuple<Values..., error::bad_index>;

public:
    using tag_t_list_instance = std::true_type;
    static constexpr std::size_t size = sizeof...(Values);

    template<std::size_t Index>
    using at = std::tuple_element_t<(Index < size) ? Index : size, NullTerminatedValuesTuple>;

    using first = at<0>;

    template<typename Value>
    static constexpr bool contains = algorithm::contains_v<Value, Values...>;

    template<tagt_templatename Template>
    static constexpr bool contains_instantiation_of = algorithm::contains_instantiation_of_v<Template, Values...>;

    template<tagt_templatename Predicate>
    static constexpr bool any_of = algorithm::any_of_v<Predicate, Values...>;

    template<tagt_templatename Predicate>
    static constexpr bool all_of = algorithm::all_of_v<Predicate, Values...>;

    template<typename... NewValues>
    using prepend = list<NewValues..., Values...>;

    template<typename... NewValues>
    using append = list<Values..., NewValues...>;

    template<typename Other>
        requires(type_traits::is_instantiation_of_v<Other, list>)
    using concatenate = typename Other::template prepend<Values...>;

    template<tagt_templatename Predicate>
    using filter = algorithm::filter_t<Predicate, list, Values...>;

    template<tagt_templatename Transformation>
    using transform_plain = algorithm::transform_plain_t<Transformation, list, Values...>;

    template<tagt_templatename Transformation>
    using transform_unwrap = algorithm::transform_unwrap_t<Transformation, list, Values...>;

    template<tagt_templatename Transformation>
    using transform = algorithm::transform_t<Transformation, list, Values...>;

    // For prettier type exuality tests
    template<typename Other>
    constexpr bool operator==(Other) const
    {
        return std::is_same_v<list<Values...>, Other>;
    }

    template<typename Other>
    constexpr bool operator!=(Other other) const
    {
        return !operator==(other);
    }
};

inline list<> empty_list;

template <typename ...Lists>
requires (algorithm::all_of_v<is_list, Lists...>)
auto join_impl(list<Lists...>) -> algorithm::concat_t<list, Lists...>;

template <list_like ListOfLists>
using join = decltype(join_impl(std::declval<ListOfLists>()));

} // namespace tag_t

#endif // include guard
